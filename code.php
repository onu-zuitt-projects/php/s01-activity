<?php

//GET FULL ADDRESS
function getFullAddress($country, $city, $province, $street){
	return "$street, $province, $city, $country";
}    

//GET LETTER GRADE
function getLetterGrade($computeGrade){
	if($computeGrade >=0 && $computeGrade <= 100){

		if($computeGrade >= 98 && $computeGrade <= 100){
		return $computeGrade.' is equivalent to A+';
		}
		
		else if ($computeGrade >= 95 && $computeGrade <= 97){
		return $computeGrade.' is equivalent to A';	
		}
		
		else if($computeGrade >= 92 && $computeGrade <= 94){
		return $computeGrade.' is equivalent to A-';
		}
		
		else if($computeGrade >= 89 && $computeGrade <= 91){
		return $computeGrade.' is equivalent to B+';
		}
		
		else if($computeGrade >= 88 && $computeGrade <= 88){
		return $computeGrade.' is equivalent to B';
		}
		
		else if($computeGrade >= 83 && $computeGrade <= 85){
		return $computeGrade.' is equivalent to B-';
		}
		
		else if($computeGrade >= 80 && $computeGrade <= 82){
		return $computeGrade.' is equivalent to C+';
		}
		
		else if($computeGrade >= 77 && $computeGrade <= 79){
		return $computeGrade.' is equivalent to C';
		}
		
		else if($computeGrade >= 75 && $computeGrade <= 76){
		return $computeGrade.' is equivalent to C-';
		}
		
		else{
		return $computeGrade.' is equivalent to D';
		}
	
	}

	else{
		return 'Invalid output. Grade must be between 0 and 100';
	}

}