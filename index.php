<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics Activity</title>
	</head>
	<body>

		<h1>Full Address</h1>
		<p><?php echo getFullAddress("Nigeria", "Lagos", "Victoria Island", "255 Walter Carrington Crescent"); ?></p>

		<h2>Letter-Based Grading</h2>
		<p><?php echo getLetterGrade(74); ?></p>
	
	</body>
</html>
